# Hydaelyn

This is an Ansible script that provisions the servers to run Shiva and Ifrit properly.

## Requirements

- A fresh Ubuntu instance
- SSH access to root
- Python is installed (`sudo apt-get install python-minimal` on Ubuntu 16.04)

## Usage

```
$ git clone git@gitlab.com:ciphr/hydaelyn.git
$ cd hydaelyn

# To provision the Production environment
$ provision/production/provision.sh


# To provision the Staging environment
$ provision/staging/provision.sh
```
